# srn-cfd-web

This repository runs a web application for predicting the super resolution output using the trained model. It makes use of the [Flask](https://flask.palletsprojects.com/en/3.0.x/) python library as an interface between HTML and python. For now, the prediction generates a server on your local machine and makes the computation. The remote computing platforms like AWS, Azure, Google Cloud Computing (GCP) or Heroku are all paid/require credit card information. Nevertheless, this repo shows a working model for the webpage.   

## Running the website locally

1. CLone the repository
2. Install the packages from the requirements.txt file
3. Run the [app.py](app.py) file from inside your virtual environment
4. After running this file, you will see a https link in the terminal, e.g., http://127.0.0.1:5000
5. Open this link in the browser
6. In the browser, click on choose file. For testing, I have provided 2 sample files inside the folder [00_test_points](00_test_points/)
7. Click on predict. After the calculation is done, a line will show up on the webpage where you can download the prediction file.
8. As backup, the predictions are also saved in the [predictions](predictions/) folder 


## Things to Note

- The input files are currently in npy format only. THe shape of each test input is (n, 512, 512, 2), where n is the number of test examples
- Loading the website the first time might take some time, because it needs to load the tensorflow and other libraries

![Instructions](docs/instructions.gif)
